# fun-with-swagger-openapi

## Usage local

```shell
$> make || make help;

Please use `make <target>`
👉       where <target> is one of
• swagger-validation-local        Pour valider le fichier swagger en local, avec docker
• swagger-validation-online       Pour valider le fichier swagger en ligne
```

### pré-requis

- cURL
- docker
- jq

## En CI

- La seule étape `validation-swagger` envoie le fichier swagger via `cURL` au `service gitlab` de validation
- La route `/validator/debug` permet d'avoir du feedback si le lint échoue
- La route `/validator` permet de générer un badge en cas d'erreur ou de succès
  - Ce badge est référencé dans la configuration de ce repo : `Settings > General > Badges`
    - ou consultable là pour l'état du swagger pour la branche main : ![](https://gitlab.com/mhouacine/fun-with-swagger-openapi/-/jobs/artifacts/main/raw/badge.png?job=validation-swagger)
