SHELL := /bin/bash
.SHELLFLAGS = -ec
.SILENT:
MAKEFLAGS += --silent
.ONESHELL:
default: help

help:
	echo -e "Please use \`make \033[36m<target>\033[0m\`"
	echo -e "👉\t where \033[36m<target>\033[0m is one of"
	grep -E '^\.PHONY: [a-zA-Z_-]+ .*?## .*$$' $(MAKEFILE_LIST) \
		| sort | awk 'BEGIN {FS = "(: |##)"}; {printf "• \033[36m%-30s\033[0m %s\n", $$2, $$3}'

.PHONY: swagger-validation-local  ## Pour valider le fichier swagger en local, avec docker
swagger-validation-local:
	docker rm -f swagger-validator-v2
	docker run -d -p 8080:8080 --name swagger-validator-v2 swaggerapi/swagger-validator-v2:v2.0.8
	sleep 4
	curl -X POST --data-binary @pet-store-swagger.yaml -H 'Content-Type:application/yaml' http://localhost:8080/validator/debug --output -
	curl -X POST --data-binary @pet-store-swagger.yaml -H 'Content-Type:application/yaml' http://localhost:8080/validator --output badge.png

.PHONY: swagger-validation-online  ## Pour valider le fichier swagger en ligne
swagger-validation-online:
	echo "[*] Lint du fichier swagger ..."
	curl -L -X POST --data-binary @pet-store-swagger.yaml -H 'Content-Type:application/yaml' https://validator.swagger.io/validator/debug --output -
	echo "[*] Génération du badge de validation ..."
	curl -X POST --data-binary @pet-store-swagger.yaml -H 'Content-Type:application/yaml' https://validator.swagger.io/validator --output badge.png
	echo "[*] Badge généré @ file://$(PWD)/badge.png"
